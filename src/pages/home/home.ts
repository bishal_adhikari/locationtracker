import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Injectable} from '@angular/core'
import {Http, Headers, HttpModule} from "@angular/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import {App, Platform} from "ionic-angular";
import {Storage} from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { SingletonService } from '../../app/services/singleton.service';


@Injectable()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  http:any;
  baseUrl:String;
  latitude:any;
  longitude:any;

  status:String;
  response:String;
  user_data:any;

  

  constructor(http:Http,public platform: Platform,private geolocation:Geolocation, public singletonService:SingletonService,private storage: Storage ){
      this.http=http;  
      this.storage.get('user_data').then((user_data) => {
        this.user_data=user_data;
      });
      this.platform.ready().then(() => {
        this.locateUser();
  
      });
  }

  locateUser(){
      this.status='Location Change detected'
      const subscription = this.geolocation.watchPosition()
      // .filter((p) => p.coords !== undefined) //Filter Out Errors
      .subscribe(position => {

        this.latitude= position.coords.latitude;
        this.longitude=position.coords.longitude;
        this.postLocation(position).subscribe(response =>
          {
            this.response=response._body;
        }
      
        );;
      
      });
  
    }
    postLocation(position){
  
      
      let location={
        current_longitude:position.coords.longitude,
        current_latitude:position.coords.latitude,
        driver_id:this.user_data.driver_id,
      };

      let headers=new Headers();

      headers.append('Content-Type', 'application/json' );

      return this.http.post(this.singletonService.baseUrl+'update/latitude-longitude',JSON.stringify(location),{headers:headers})
      .map((res=>res));
      
    }


}
