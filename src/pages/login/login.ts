import { HomePage } from './../home/home';
import { AppService } from './../../app/services/app.service';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @ViewChild('email') email;
  @ViewChild('password') password;

  token:any;
  user_data:any={};
  message:any;
  print:any;
  remember_token:any;
  response:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private appService:AppService,private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  login(){

         let credentials={
           username:this.email,
           password:this.password
         };
   
     this.appService.authenticate(credentials).subscribe(response =>
       {
        this.user_data=response; 

         this.storage.set('user_data', this.user_data);
   
   
        console.log(response);
        this.navCtrl.push(HomePage);

   
   
       });

     }
}
