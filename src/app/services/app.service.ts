import { SingletonService } from './singleton.service';
import {Injectable} from '@angular/core'
import {Http, Headers} from "@angular/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import {App, Platform} from "ionic-angular";
// import {Storage} from '@ionic/storage';

@Injectable()

export class AppService{
    http:any;
    baseUrl:String;
    items:any;
    public categories;
    public logged_in: boolean;
    public user_data: any;
    remember_token: any;




    constructor(http:Http,public singletonService:SingletonService,private platform:Platform,private app:App){
        this.http=http;
        this.baseUrl=singletonService.baseUrl;

  
    //     this.get_logged_in_user().subscribe(response => {
    //       this.user_data = response;
    //       this.logged_in =true;
    //     });
      
    //   console.log(this.remember_token);

    }

    get_user_data(){
      return this.user_data;
    }
    get_logged_in_user(){

           return this.getUser(this.remember_token);
            // if(this.user_data){
            //     return true;

            // }

    }

    authenticate(credentials){
        let headers=new Headers();
        headers.append('Content-Type','application/json');
        return this.http.post(this.baseUrl+'login',JSON.stringify(credentials),{headers:headers})
        .map(res=>res.json());

    }
    getUser(token){

            let headers=new Headers();
            // headers.append('Content-Type', 'application/json' );
            headers.append('Authorization','Bearer '+ token);
            return this.http.get(this.baseUrl+'user',{headers:headers},{timeout: 5000}).map(res=>res.json());






    }
    register(credentials){
        let headers=new Headers();
        headers.append('Content-Type','application/json');
        return this.http.post(this.baseUrl+'auth/register',JSON.stringify(credentials),{headers:headers})
        .map(res=>res.json());

    }




}
